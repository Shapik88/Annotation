package com;

import java.util.Base64;
import java.util.Scanner;

public class Main {

    private static String userPassword;

    public static void main(String[] args) throws IllegalAccessException {
        //System.out.println(new String(Base64.getEncoder().encode("Shapik".getBytes())));
        Scanner scanner = new Scanner(System.in);
        Encoding encoding = new Encoding();
        System.out.println("Enter user name:");
        String login =  scanner.next();
        System.out.println("Enter password:");
        String password = scanner.next();
        User user = new User(login, password);
        encoding.encrypt(user);

        if (encoding.checkPassword(user)&& encoding.checkUserName(user)){
            System.out.println("User name and Password successful!");
        }
        else {
            System.out.println("User name or Password failed!");
        }
    }
}