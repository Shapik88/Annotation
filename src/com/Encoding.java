package com;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Base64;

/**
 * Created by Евгений on 14.07.2017.
 */
public class Encoding {
    protected final String CODE = "U2hhcGlr";
    protected final String CODE2 = "U2hhcGlr";



    public void encrypt(User user) throws IllegalAccessException {
        Field[] fields = user.getClass().getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation: annotations) {
                if (annotation instanceof Encrypted){
                    String password = user.getPassword();
                    field.setAccessible(true);
                    field.set(user, new String(Base64.getEncoder().encode(password.getBytes())));
                }
            }
            for (Annotation annotation: annotations) {
                if (annotation instanceof Encrypted){
                    String userName = user.getUserName();
                    field.setAccessible(true);
                    field.set(user, new String(Base64.getEncoder().encode(userName.getBytes())));
                }
            }
        }
    }
    public boolean checkPassword(User user){

        return user.getPassword().equals(CODE);
    }
    public boolean checkUserName(User user){

        return user.getPassword().equals(CODE2);
    }



}