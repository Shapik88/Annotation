package com;

/**
 * Created by Евгений on 14.07.2017.
 */
public class User {

    @Encrypted
    private String password;
    private String userName;

    User(String userName, String password){
        this.userName = userName;
        this.password = password;
    }
    public String getPassword(){

        return password;
    }
    public String getUserName(){
        return userName;
    }
}


