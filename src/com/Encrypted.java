package com;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Евгений on 14.07.2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Encrypted {


}
